const Discord = require("discord.js");
const fs = require("fs");
const path = require("path");
const request = require("request");
const listenStreams = new Map();
const deepspeech = require("./deepspeech.js");
const process = require("process");
const cwd = process.cwd();
const Sox = require("sox-stream");
const config = JSON.parse(fs.readFileSync("./settings.json", "utf-8"));
const modelPath = cwd + config.model.path;

require('events').EventEmitter.defaultMaxListeners = 1500;

//const recordingsPath = fs.mkdirSync(config.recordingsPath);
const discordClient = new Discord.Client();

discordClient.login(config.discordToken).catch((e) => {
  console.log(e, config.discordToken);
});

discordClient.on("ready", () => {
  discordClient.user.setActivity(config.activityMessage);
});

function generateOutputFile(channel, member) {
  // use IDs instead of username cause some people have stupid emojis in their name
  var fileName = `./recordings/${channel.id}-${member.id}-${Date.now()}.raw`;
  return fs.createWriteStream(fileName);
}

discordClient.on("message", (message) => {
  if(message.author.bot) return;
  if(message.content.indexOf(config.prefix) !== 0) return;

  var args = message.content.split(/\s+/);
  var command = args.shift().replace(config.prefix, '');

  if (command === 'listen') {
    var member = message.member;
    if (!member) {
      return;
    }
    if (!member.voiceChannel) {
      message.reply(" you need to be in a voice channel first.")
      return;
    }

    var voiceChannel = member.voiceChannel;
    console.log('Listening in to **' + member.voiceChannel.name + '**!');

    voiceChannel.join().then((connection) => {
      connection.on("speaking", (member, speaking) => {
        var receiver = connection.createReceiver();
        if (speaking) {
          console.log(member.id, member.username, speaking);
          //message.channel.sendMessage(`I'm listening to ${member}`);
          // this creates a 16-bit signed PCM, stereo 48KHz PCM stream.
          const audioStream = receiver.createPCMStream(member);
          // create an output stream so we can dump our data in a file
          const outputStream = generateOutputFile(voiceChannel, member);
          // const soxConfig = {
          //   input: { type: 'raw', rate: 48000, channels: 2, bits: 16, encoding: 'signed-integer' },
          //   output: { bits: 16, rate: 16000, channels: 1, type: 'raw' }
          // };
          // pipe our audio data into the file stream
          audioStream
            // .pipe(Sox(soxConfig))
            .pipe(outputStream);

          console.log(outputStream.path);

          // when the stream ends (the user stopped talking) tell the user
          audioStream.on("end", () => {
            //message.channel.sendMessage(`I'm no longer listening to ${member}`);
          });

          outputStream.on("finish", () => {
            fs.stat(outputStream.path, (err, stats) => {
              if (!err && stats.size > 10000 ) {
                deepspeech.speechToText(outputStream.path, (text) => {
                  if (text) {
                    console.log(member.username, text, speaking);
                    message.channel.sendMessage(`${member}: ${text}`);
                  }
                  fs.unlink(outputStream.path, (err) => {
                    console.trace(err);
                  });
                });
              }
              else {
                fs.unlink(outputStream.path, err => console.error);
              }
            });
          });
        }
      });
    }).catch(console.error);
  }

});

discordClient.on("voiceStateUpdate", (oldMember, newMember) => {
  console.log(oldMember.id, oldMember.user.username, newMember);
});
