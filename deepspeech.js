"use strict";
const deepspeech = require("deepspeech");
const fs = require("fs");
const MemoryStream = require("memory-stream");
const Sox = require("sox-stream");
const process = require("process");
const cwd = process.cwd();
const config = JSON.parse(fs.readFileSync("./settings.json", "utf-8"));
const modelPath = cwd + config.model.path;

class DeepSpeechInstance {
  constructor() {
    console.log('this');
    this.model = this.loadModel();
  }

  loadModel() {
    console.error('Loading model from file %s', modelPath + config.model.pb);
    const model_load_start = process.hrtime();

    var model = new deepspeech.Model(modelPath + config.model.pb, 26, 9, modelPath + config.model.alphabet, 500);

    const model_load_end = process.hrtime(model_load_start);
    console.error('Loaded model in %ds.', this.totalTime(model_load_end));

    console.error('Loading language model from files %s %s', modelPath + config.model.lm, modelPath + config.model.trie);
    const lm_load_start = process.hrtime();

    model.enableDecoderWithLM(modelPath + config.model.alphabet, modelPath + config.model.lm, modelPath + config.model.trie, 1.75, 1.00, 1.00);

    const lm_load_end = process.hrtime(lm_load_start);
    console.error('Loaded language model in %ds.', this.totalTime(lm_load_end));

    return model;
  }

  totalTime(hrtimeValue) {
    return (hrtimeValue[0] + hrtimeValue[1] / 1000000000).toPrecision(4);
  }

  speechToText(audioFile, callback) {
    var audioStream = new MemoryStream();
    var model = this.model;
    var totalTime = this.totalTime;

    fs.createReadStream(audioFile).
      pipe(Sox({
        input: {type: 'raw', rate: 48000, channels: 2, bits: 16, encoding: 'signed-integer'},
        output: { bits: 16, rate: 16000, channels: 1, type: 'raw' } }).on("error", (err) => {
          console.trace(err);
      })).
      pipe(audioStream);

    audioStream.on("finish", () => {
      var audioBuffer = audioStream.toBuffer();

      const inference_start = process.hrtime();
      console.error('Running inference.');
      const audioLength = (audioBuffer.length / 2) * ( 1 / 16000);

      callback(model.stt(audioBuffer.slice(0, audioBuffer.length / 2), 16000));

      const inference_stop = process.hrtime(inference_start);
      console.error('Inference took %ds for %ds audio file.', totalTime(inference_stop), audioLength.toPrecision(4));
    });
  }
}

module.exports = new DeepSpeechInstance();
